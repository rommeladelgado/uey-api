import { GraphQLString,GraphQLInt } from 'graphql'
import {
    getAllProfitableProducts,
    getProfitableProductById
} from '../../repositories/profitable-type-product.repository';

import {
    DataProfitableProductsType,
    ProfitableProductType
} from "../type-defs/profitable-type-product";

export const GET_ALL_PROFITABLE_PRODUCTS = {
    type: DataProfitableProductsType,
    args: {
        page: { type: GraphQLInt },
        rowsPerPage: { type: GraphQLInt },
        search: { type: GraphQLString }
    },
     resolve: (_: any, args: any) => getAllProfitableProducts({...args})
}

export const GET_PROFITABLE_PRODUCT_BY_ID = {
    type: ProfitableProductType,
    args: {
        id: { type: GraphQLInt },
    },
    resolve: (_: any, args: any)  => getProfitableProductById(args.id)
}