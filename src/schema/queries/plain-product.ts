import { GraphQLString,GraphQLInt } from 'graphql'
import {getAllPlainProducts, getPlainProductById} from '../../repositories/plain-product.repository';

import { DataPlainProductsType, PlainProductsType } from '../type-defs/plain-product';

export const GET_ALL_PLAIN_PRODUCTS = {
    type: DataPlainProductsType,
    args: {
        page: { type: GraphQLInt },
        rowsPerPage: { type: GraphQLInt },
        search: { type: GraphQLString }
    },
    resolve: (_: any, args: any)  => getAllPlainProducts({...args})
}

export const GET_PLAIN_PRODUCT_BY_ID = {
    type: PlainProductsType,
    args: {
        id: { type: GraphQLInt },
    },
    resolve: (_: any, args: any)  => getPlainProductById(args.id)
}