import {GraphQLString, GraphQLInt} from 'graphql'
import {DataSpaceProductsType, SpaceProductType} from "../type-defs/space-type-product";
import {getAllSpaceTypeProducts, getSpaceTypeProductById} from "../../repositories/space-product-type.repository";

export const GET_ALL_SPACE_TYPE_PRODUCTS = {
    type: DataSpaceProductsType,
    args: {
        page: { type: GraphQLInt },
        rowsPerPage: { type: GraphQLInt },
        search: { type: GraphQLString }
    },
    resolve: (_: any, args: any) => getAllSpaceTypeProducts({...args})
}

export const GET_SPACE_TYPE_PRODUCT_BY_ID = {
    type: SpaceProductType,
    args: {
        id: { type: GraphQLInt },
    },
    resolve: (_: any, args: any)  => getSpaceTypeProductById(args.id)
}