import { GraphQLObjectType, GraphQLString,GraphQLList, GraphQLID, GraphQLFloat, GraphQLInt } from 'graphql'

export const PlainProductsType = new GraphQLObjectType({
    name: 'PlainProductType',
    fields: {
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        price: { type: GraphQLFloat },
        description: { type: GraphQLString },
        image: { type: GraphQLString },
        seller: { type: GraphQLString },
        stock: { type: GraphQLInt },
    }
})

export const DataPlainProductsType = new GraphQLObjectType({
    name: 'DataPlainProductsType',
    fields: () => ({
        count: {
            type: GraphQLInt
        },
        list: {type: new GraphQLList(PlainProductsType)}
    })
})

