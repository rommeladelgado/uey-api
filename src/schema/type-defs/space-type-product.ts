import { GraphQLObjectType, GraphQLString,GraphQLList, GraphQLID, GraphQLFloat, GraphQLInt } from 'graphql'


const ReservationType = new GraphQLObjectType({
    name: 'SpaceReservation',
    fields: {
        id: { type: GraphQLID },
        reservationDate: { type: GraphQLString }
    }
});

export const SpaceProductType = new GraphQLObjectType({
    name: 'SpaceProductType',
    fields: {
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        price: { type: GraphQLFloat },
        description: { type: GraphQLString },
        image: { type: GraphQLString },
        seller: { type: GraphQLString },
        latitude: { type: GraphQLFloat },
        longitude: { type: GraphQLFloat },
        reservations: { type: GraphQLList(ReservationType) },
    }
})


export const DataSpaceProductsType = new GraphQLObjectType({
    name: 'DataSpaceProductType',
    fields: () => ({
        count: {
            type: GraphQLInt
        },
        list: {type: new GraphQLList(SpaceProductType)}
    })
})