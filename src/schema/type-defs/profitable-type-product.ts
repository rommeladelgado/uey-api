import { GraphQLObjectType, GraphQLString,GraphQLList, GraphQLID, GraphQLFloat, GraphQLInt } from 'graphql'


const ReservationType = new GraphQLObjectType({
    name: 'ReservationType',
    fields: {
        id: { type: GraphQLID },
        reservationDate: { type: GraphQLString },
        rentType: { type: GraphQLString }
    }
});

export const ProfitableProductType = new GraphQLObjectType({
    name: 'ProfitableProductType',
    fields: {
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        price: { type: GraphQLFloat },
        description: { type: GraphQLString },
        image: { type: GraphQLString },
        seller: { type: GraphQLString },
        reservations: { type: GraphQLList(ReservationType) },
    }
})


export const DataProfitableProductsType = new GraphQLObjectType({
    name: 'DataProfitableProductType',
    fields: () => ({
        count: {
            type: GraphQLInt
        },
        list: {type: new GraphQLList(ProfitableProductType)}
    })
})