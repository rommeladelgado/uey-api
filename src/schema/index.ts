import { GraphQLSchema, GraphQLObjectType } from 'graphql'
import {GET_ALL_PLAIN_PRODUCTS, GET_PLAIN_PRODUCT_BY_ID} from './queries/plain-product'
import {GET_ALL_PROFITABLE_PRODUCTS, GET_PROFITABLE_PRODUCT_BY_ID} from './queries/profitable-type-product'
import {GET_ALL_SPACE_TYPE_PRODUCTS, GET_SPACE_TYPE_PRODUCT_BY_ID} from './queries/space-type-product'

const RootQuery = new GraphQLObjectType({
    name: 'RootQuery',
    fields: {
        getAllPlainProducts: GET_ALL_PLAIN_PRODUCTS,
        getPlainProductById: GET_PLAIN_PRODUCT_BY_ID,
        getAllProfitableProducts: GET_ALL_PROFITABLE_PRODUCTS,
        getProfitableById: GET_PROFITABLE_PRODUCT_BY_ID,
        getAllSpaceProducts: GET_ALL_SPACE_TYPE_PRODUCTS,
        getSpaceTypeProductById: GET_SPACE_TYPE_PRODUCT_BY_ID,
    }
})
export const schema = new GraphQLSchema({
    query: RootQuery,
})