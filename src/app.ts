import express from 'express'
import { graphqlHTTP } from 'express-graphql'
import { schema } from './schema'
import cors from 'cors';
import dotenv from "dotenv";
import path from "path";
const app = express()

dotenv.config({
    path: path.join(__dirname, '../', '.env')
})

app.use(cors({ origin: '*'}))
app.use('/graphql', graphqlHTTP({
    graphiql: true,
    schema
}))

export default app