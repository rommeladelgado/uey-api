import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, BaseEntity } from 'typeorm'
import { Seller } from './seller.entity'

@Entity()
export class PlainProduct extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    description: string

    @Column()
    image: string

    @Column('float')
    price: number

    @Column()
    stock: string

    
    @ManyToOne(
        () => Seller, 
        (seller) => seller.plainProducts)
    seller: Seller
}