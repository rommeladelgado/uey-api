import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm'

import { SpaceTypeProduct } from './space-type-product.entity'

export enum RentType {
    NIGHT = 'night',
    HOUR = 'hour'
}
@Entity()
export class SpaceReservation {
    @PrimaryGeneratedColumn()
    id: number

    @Column('date')
    reservationDate: string


    @ManyToOne(
        () => SpaceTypeProduct, 
        (spaceTypeProduct) => spaceTypeProduct.reservations
    )
    spaceTypeProduct: SpaceTypeProduct

}