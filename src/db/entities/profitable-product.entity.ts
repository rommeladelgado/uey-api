import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm'
import { ProfitableProductReservation } from './profitable-product-reservation.entity'
import { Seller } from './seller.entity'

@Entity()
export class ProfitableProduct {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    description: string

    @Column()
    image: string

    @Column('float')
    price: number

    @OneToMany(
        () => ProfitableProductReservation, 
        (reservation) => reservation.profitableProduct)
    reservations: ProfitableProductReservation[]
    
    @ManyToOne(
        () => Seller, 
        (seller) => seller.profitableProducts
    )
    seller: Seller
}