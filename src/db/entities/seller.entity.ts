import { Entity, PrimaryGeneratedColumn, Column,OneToMany, BaseEntity } from 'typeorm'
import { PlainProduct } from './plain-product.entity'
import { ProfitableProduct } from './profitable-product.entity'
import { SpaceTypeProduct } from './space-type-product.entity'

@Entity()
export class Seller extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    lastname: string

    @OneToMany(
        () => PlainProduct, 
        (plainProduct) => plainProduct.seller
    )
    plainProducts: PlainProduct[]

    

    @OneToMany(
        () => SpaceTypeProduct, 
        (spaceType) => spaceType.seller
    )
    spaceTypeProducts: SpaceTypeProduct[]

    @OneToMany(
        () => ProfitableProduct, 
        (profitable) => profitable.seller
    )
    profitableProducts: ProfitableProduct[]
}