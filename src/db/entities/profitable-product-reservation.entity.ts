import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, BaseEntity} from 'typeorm'
import  { ProfitableProduct } from './profitable-product.entity'

export enum RentType {
    NIGHT = 'night',
    HOUR = 'hour'
}
@Entity()
export class ProfitableProductReservation extends  BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column('date')
    reservationDate: string

    @Column({
        type: 'enum',
        enum: RentType,
        default: RentType.NIGHT
    })
    rentType: RentType
    
    @ManyToOne(
        () => ProfitableProduct, 
        (profitableProduct) => profitableProduct.reservations
    )
    profitableProduct: ProfitableProduct

}