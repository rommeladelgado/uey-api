import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, BaseEntity } from 'typeorm'
import { Seller } from './seller.entity'
import { SpaceReservation } from './space-reservation.entity'

@Entity()
export class SpaceTypeProduct extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    image: string

    @Column()
    description: string

    @Column('float')
    price: number

    @Column('double')
    latitude: number

    @Column('double')
    longitude: number

    @OneToMany(
        () => SpaceReservation,
        (reservation) => reservation.spaceTypeProduct)
    reservations: SpaceReservation[]

    @ManyToOne(
        () => Seller, 
        (seller) => seller.spaceTypeProducts)
    seller: Seller
    
}