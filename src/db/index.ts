import { DataSource } from 'typeorm'
import { PlainProduct } from './entities/plain-product.entity'
import { ProfitableProduct } from './entities/profitable-product.entity'
import { ProfitableProductReservation } from './entities/profitable-product-reservation.entity'
import { Seller  } from './entities/seller.entity'
import { SpaceTypeProduct } from './entities/space-type-product.entity'
import { SpaceReservation } from './entities/space-reservation.entity'
import { SnakeNamingStrategy } from 'typeorm-naming-strategies'

export const AppDataSource = new DataSource({
    type: "mysql",
    host: "sql901.main-hosting.eu",
    port: 3306,
    username: "u263279241_ueyroot",
    password: "uey-Events23",
    database: "u263279241_uey",
    entities: [
        Seller,
        PlainProduct,
        ProfitableProduct,
        ProfitableProductReservation,
        SpaceTypeProduct,
        SpaceReservation
    ],
    ssl: false,
    synchronize: true,
    namingStrategy: new SnakeNamingStrategy()
})

