import app from './app'
import {AppDataSource} from "./db";



async function main() {
    try {

        await AppDataSource.initialize()
        app.listen(process.env.APP_PORT, () => {})
    } catch (err) {
        console.error('Error connection DB')
    }
}

main()
