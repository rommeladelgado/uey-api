import { AppDataSource} from "../db";
import { SpaceTypeProduct } from "../db/entities/space-type-product.entity";

type Arguments = {
    search: string,
    page: number,
    rowsPerPage: number,
}

export const getAllSpaceTypeProducts = async (args: Arguments) => {

    const query = await AppDataSource
        .getRepository(SpaceTypeProduct)
        .createQueryBuilder('product')
        .where('product.name like :search', {search: `%${args.search}%`});
        //
        const count = await query.getMany();

        const response =  await query
        .innerJoinAndSelect('product.seller', 's')
        .leftJoinAndSelect('product.reservations', 'r')
            .take(args.rowsPerPage)
            .skip(args.page * args.rowsPerPage)
        .getMany();

        const list = response.map((el) => ({...el, seller: `${el.seller.name} ${el.seller.lastname}`}));
        return {
            count: count.length,
            list,
        }
}

export const getSpaceTypeProductById = async (id: number) => {
    const response = await AppDataSource
        .getRepository(SpaceTypeProduct)
        .createQueryBuilder('product')
        .innerJoinAndSelect('product.seller', 's')
        .leftJoinAndSelect('product.reservations', 'r')
        .where('product.id = :prodId', {prodId: id})
        .getOne();

    if (response) {
        return {
            ...response,
            seller: `${response.seller.name} ${response.seller.lastname}`
        }
    }
    return null
}