import { AppDataSource } from "../db";
import { PlainProduct } from "../db/entities/plain-product.entity";

type Arguments = {
    search: string,
    page: number,
    rowsPerPage: number,
}

export const getAllPlainProducts = async (args: Arguments) => {

    const query = await AppDataSource
        .getRepository(PlainProduct)
        .createQueryBuilder('pr')
        .where('pr.name like :search', {search: `%${args.search}%`});

        const count = await query.getMany();

        const response =  await query
        .innerJoinAndSelect('pr.seller', 's')
        .take(args.rowsPerPage)
        .skip(args.page * args.rowsPerPage)
        .getMany();

        const list = response.map((el) => ({...el, seller: `${el.seller.name} ${el.seller.lastname}`}));
        return {
            count: count.length,
            list,
        }
}

export const getPlainProductById = async (id: number) => {
    const response = await AppDataSource
        .getRepository(PlainProduct)
        .createQueryBuilder('pr').
        innerJoinAndSelect('pr.seller', 's')
        .where('pr.id = :prodId', {prodId: id})
        .getOne()
    if (response) {
        return {
            ...response,
            seller: `${response.seller.name} ${response.seller.lastname}`
        }
    }
    return null
}