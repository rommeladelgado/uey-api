export interface ConnectionDBType {
    username: string
    password: string
    host: string
    port: number
    dialect: string
    database: string
}